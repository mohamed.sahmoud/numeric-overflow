package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Security unit tests")
@Tag("security")
public class appSecuritySpec {

    @Test
    public void doesNotAllowCreationOfAmountThatIsNotInRange() {
        assertThrows(IllegalArgumentException.class, () -> new Amount(BigInteger.valueOf(-1)));
        assertThrows(IllegalArgumentException.class, () -> new Amount(BigInteger.ZERO));
        assertThrows(IllegalArgumentException.class, () -> new Amount(BigInteger.valueOf(2147483647).add(BigInteger.ONE)));
        assertThrows(IllegalArgumentException.class, () -> new Amount(BigInteger.valueOf(-2147483648).subtract(BigInteger.ONE)));
    }
}
