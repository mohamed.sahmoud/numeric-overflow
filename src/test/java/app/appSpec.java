package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Usability unit tests")
public class appSpec {

    @Test
    public void ThousandsNeedsApproval() {
        Main app = new Main();
        boolean res = app.approval(1000);
        assertTrue(res,() -> "1000 needs approval");
    }

    @Test
    public void FileHundredsDoesNotNeedApproval() {
        Main app = new Main();
        boolean res = app.approval(500);
        assertFalse(res,() -> "500 does not need approval");
    }
}
