package app;

import java.math.BigInteger;

public class Main {

    private Amount threshold = new Amount(BigInteger.valueOf(1000));

    public static void main(String[] args) {

        Main app = new Main();

        int amount = 999;
        if (!app.approval(amount)) {
            System.out.println(amount + " does not require approval");
        }
        amount = 2000;
        if (app.approval(amount)) {
            System.out.println(amount + " requires approval");
        }
    }

    public boolean approval(long amount) {
        return !(new Amount(BigInteger.valueOf(amount)).lessThan(threshold));
    }
}
