package app;

import java.math.BigInteger;

public class Amount {

    private static final BigInteger MAXIMUM_AMOUNT = BigInteger.valueOf(Integer.MAX_VALUE);

    private final int value;

    public Amount(BigInteger value) {
        if (value == null || !BigInteger.ONE.min(value).equals(BigInteger.ONE) ||
                !MAXIMUM_AMOUNT.max(value).equals(MAXIMUM_AMOUNT)) {
            throw new IllegalArgumentException("Invalid amount");
        }
        this.value = value.intValue();
    }

    public boolean lessThan(Amount otherAmount) {
        return this.value < otherAmount.value;
    }
}
